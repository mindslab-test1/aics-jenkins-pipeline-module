#!/usr/bin/env groovy

def getCommit() {
    sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%h'").trim()
}

def getTag() {
    sh(returnStdout: true, script: 'git describe --tags $(git rev-list --tags --max-count=1)').trim()
}

def getAuthor() {
    sh(returnStdout: true, script: "git --no-pager show -s --format='%an' ${env.GIT_COMMIT}").trim()
}

def getMessage() {
    sh(returnStdout: true, script: 'git log -1 --pretty=%B').trim()
}